package com.ma.user1.shapehover;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;


public class MainActivity extends Activity {

    private View whiteView;
    private TextView titleTextView;
    private TextView detailTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        whiteView = findViewById(R.id.whiteView);

        titleTextView = (TextView) findViewById(R.id.titleTextView);

        detailTextView = (TextView) findViewById(R.id.detailTextView);

        whiteView.setBackground(new Polygon());

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    void startAnimation() {

        AnimatorSet animatorSet = new AnimatorSet();

        ObjectAnimator translationTitleX = ObjectAnimator.ofFloat(titleTextView, "translationX", 0);
        ObjectAnimator translationTitleY = ObjectAnimator.ofFloat(titleTextView, "translationY", -(titleTextView.getY()));



        ObjectAnimator translationDetailViewX = ObjectAnimator.ofFloat(detailTextView, "translationX", 0);
        ObjectAnimator translationDetailViewY = ObjectAnimator.ofFloat(detailTextView, "translationY", -(titleTextView.getY() - 20));

        ObjectAnimator scaleDetailViewX = ObjectAnimator.ofFloat(detailTextView, "scaleX", 0);
        ObjectAnimator scaleDetailViewY = ObjectAnimator.ofFloat(detailTextView, "scaleY", 0.5f, 0);

        ObjectAnimator transparentDetailView = ObjectAnimator.ofFloat(detailTextView, "alpha", 1, 0);

//        ObjectAnimator translationX = ObjectAnimator.ofFloat(titleTextView, "x", 0);
//        ObjectAnimator translationY = ObjectAnimator.ofFloat(titleTextView, "y", -700);

        translationDetailViewX.setDuration(700);
        translationDetailViewY.setDuration(700);
        transparentDetailView.setDuration(500);

        translationTitleX.setDuration(800);
        translationTitleY.setDuration(800);


        ObjectAnimator pivotX = ObjectAnimator.ofFloat(whiteView, "pivotX", 100);
        ObjectAnimator pivotY = ObjectAnimator.ofFloat(whiteView, "pivotY", 0);

        ObjectAnimator scaleX = ObjectAnimator.ofFloat(whiteView, "scaleX", 1f, 1f);
        ObjectAnimator scaleY = ObjectAnimator.ofFloat(whiteView, "scaleY", 1f, 0.0f);

        scaleX.setDuration(800);
        scaleY.setDuration(800);

        animatorSet.playTogether(pivotX, pivotY, scaleX, scaleY, translationTitleX, translationTitleY, translationDetailViewX, translationDetailViewY, transparentDetailView);

//        animatorSet.play(scaleX).with(scaleY).after(animatorSet.playTogether(pivotX,pivotY));
        animatorSet.start();
//        final Animation animation1 = AnimationUtils.loadAnimation(whiteView.getContext(), R.anim.scale_to_zero);
//        whiteView.startAnimation(animation1);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            startAnimation();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

}