package com.ma.user1.shapehover;

import android.graphics.*;
import android.graphics.drawable.Drawable;

/**
 * Created by Cbishnoi on 29-01-2015.
 */
public class Polygon extends Drawable {
    private Path path;
    private Paint paint;

    public Polygon() {
        init();
    }


    private void init() {
        paint = new Paint();
        paint.setStyle(Paint.Style.FILL);
        paint.setColor(Color.WHITE);
    }

    @Override
    public void draw(Canvas canvas) {
        int height = getBounds().height();
        int width = getBounds().width();
        path = new Path();
        path.moveTo(0, 0);
        path.lineTo(width, 0);
        path.lineTo(width, height / 2);
        float newHeight = (float) (height / 1.5);
        path.lineTo(0, newHeight);
        canvas.drawPath(path, paint);
    }


    @Override
    public void setAlpha(int alpha) {
        paint.setAlpha(alpha);
    }

    @Override
    public void setColorFilter(ColorFilter cf) {
        paint.setColorFilter(cf);
    }

    @Override
    public int getOpacity() {
        return PixelFormat.TRANSLUCENT;
    }
}
